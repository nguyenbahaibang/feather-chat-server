// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const users = new mongooseClient.Schema({
    // _id: { type: String},
    first_name: { type: String, required: false },
    last_name: { type: String, required: false },
    isVerified: { type: Boolean, default: false },
    login_attempts: { type: Number, default: 0 },

    email: {type: String, unique: true},
    password: { type: String },

    googleId: { type: String },

    facebookId: { type: String },

    githubId: { type: String },
  }, {
    timestamps: true
  });

  return mongooseClient.model('users', users);
};

// messages-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
// const mongoose = require('mongoose');
import mongoose from 'mongoose';

module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const messages = new Schema({
    text: { type: String },
    userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null}
  }, {
    timestamps: true
  });

  return mongooseClient.model('messages', messages);
};
